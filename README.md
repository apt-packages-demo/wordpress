# wordpress

[wordpress](https://tracker.debian.org/pkg/wordpress) Web blogging [wordpress.org](https://wordpress.org)

# WordPress
* [*wiki.debian.org/WordPress*
  ](https://wiki.debian.org/WordPress)

![Worpress packages popularity at Debian](https://qa.debian.org/cgi-bin/popcon-png?packages=wordpress%20wordpress-l10n%20wordpress-theme-twentynineteen%20wordpress-theme-twentytwenty%20wordpress-theme-twentytwentyone&show_installed=on&want_legend=on&want_ticks=on&date_fmt=%25Y-%25m&beenhere=1)

# Installation
## openrc reload apache2
### Official documentation
* [*wiki.gentoo.org/wiki/Apache*
  ](https://wiki.gentoo.org/wiki/Apache)

### Unofficial documentation
* [*Start / Stop and Restart Apache 2 Web Server Command*
  ](https://www.cyberciti.biz/faq/star-stop-restart-apache2-webserver)
  2020-10 Vivek Gite
